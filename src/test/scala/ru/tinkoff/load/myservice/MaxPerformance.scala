package ru.tinkoff.load.myservice

import ru.tinkoff.gatling.config.SimulationConfig._
import ru.tinkoff.load.myservice.scenarios.CommonScenario
import ru.tinkoff.gatling.influxdb.Annotations
import io.gatling.core.Predef._

class MaxPerformance extends Simulation  with Annotations {

  setUp(
    CommonScenario().inject(
      incrementUsersPerSec(intensity / stagesNumber) // интенсивность на ступень
        .times(stagesNumber) // Количество ступеней
        .eachLevelLasting(stageDuration) // Длительность полки
        .separatedByRampsLasting(rampDuration) // Длительность разгона
        .startingFrom(0) // Начало нагрузки с
    )
  ).protocols(grpcProtocol)
    .maxDuration(testDuration) // общая длительность теста
}
