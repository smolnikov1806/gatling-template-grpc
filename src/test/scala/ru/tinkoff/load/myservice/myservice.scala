package ru.tinkoff.load

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.grpc.ManagedChannelBuilder
import ru.tinkoff.gatling.config.SimulationConfig._

package object myservice {

  val grpcProtocol = grpc(ManagedChannelBuilder.forAddress(baseUrl, 443))

}
