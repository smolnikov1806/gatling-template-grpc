package ru.tinkoff.load.myservice.cases

import io.gatling.http.Predef._
import io.gatling.core.Predef._
// Закомментировать код ниже
import com.github.phisgr.gatling.grpc.Predef.grpc
import com.github.phisgr.gatling.grpc.Predef._
import io.grpc.health.v1.health.HealthCheckResponse.ServingStatus.SERVING
import io.grpc.health.v1.health.{HealthCheckRequest, HealthGrpc}

object gRPCHealth {

  val request = grpc("HealthGrpc_METHOD_CHECK")
    .rpc(HealthGrpc.METHOD_CHECK)
    .payload(HealthCheckRequest.defaultInstance)
    .extract(_.status.some)(_ is SERVING)

}
